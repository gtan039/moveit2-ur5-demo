#include "rclcpp/rclcpp.hpp"
#include "rclcpp/rclcpp.hpp"
#include <memory>
#include <chrono>

#include <thread>
#include <moveit/moveit_cpp/moveit_cpp.h>
#include <moveit/moveit_cpp/planning_component.h>
#include <moveit/robot_state/conversions.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit_msgs/msg/display_robot_state.hpp>
#include <moveit/robot_model/robot_model.h>


#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>


#include <trajectory_msgs/msg/joint_trajectory.hpp>
#include <geometry_msgs/msg/pose_stamped.hpp>
#include "control_msgs/action/follow_joint_trajectory.hpp"
#include "rclcpp_action/rclcpp_action.hpp"
#include <grasp_planning/msg/grasp_pose.hpp>

static const rclcpp::Logger LOGGER = rclcpp::get_logger("grasp_execution_node");

class GraspExecutionNode
{
    public:

    GraspExecutionNode(const rclcpp::Node::SharedPtr& node)
    : node_(node)
    , robot_state_publisher_(node_->create_publisher<moveit_msgs::msg::DisplayRobotState>("display_robot_state", 1))
    {
       
    }
    
    void run()
    {
      moveit_cpp_ = std::make_shared<moveit::planning_interface::MoveItCpp>(node_);
      moveit_cpp_->getPlanningSceneMonitor()->providePlanningSceneService();  // let RViz display query PlanningScene
      moveit_cpp_->getPlanningSceneMonitor()->setPlanningScenePublishingFrequency(100);
      rclcpp::Clock::SharedPtr clock_ptr(new rclcpp::Clock(RCL_ROS_TIME));
      tfBuffer = new tf2_ros::Buffer(clock_ptr);
      tfListener = new tf2_ros::TransformListener(*tfBuffer);
      planning_sub  = node_->create_subscription<grasp_planning::msg::GraspPose>("/grasp_poses",10, std::bind(&GraspExecutionNode::execution_init, this, std::placeholders::_1));
    }
    ~GraspExecutionNode()
    {
      delete tfBuffer;
      delete arm;
      delete tfListener;
    }

    void execution_init(const grasp_planning::msg::GraspPose::SharedPtr msg)
    {
      std::string ee_link = "tool0"; 
      transformStamped = tfBuffer->lookupTransform(robot_frame, camera_frame,node_->now(),std::chrono::seconds(1000));
      arm = new moveit::planning_interface::PlanningComponent("manipulator", moveit_cpp_); ;
      //moveit::planning_interface::PlanningComponent arm("manipulator", moveit_cpp_);

      geometry_msgs::msg::PoseStamped home_pose;
      home_pose.header.frame_id = robot_frame;
      home_pose.pose = get_curr_pose(ee_link);
      //moveit::planning_interface::MoveItCppPtr moveit_cpp_;
      
      // A little delay before running the plan
      rclcpp::sleep_for(std::chrono::seconds(3));

      // ------------------- For grasp planned objects --------------------------
      std::vector<moveit_msgs::msg::CollisionObject> grasp_objects;
      moveit_msgs::msg::PlanningSceneWorld psw;
      for(int i = 0; i < int(msg->num_objects); i++)
      {
        moveit_msgs::msg::CollisionObject temp_collision_object;
        temp_collision_object.header.frame_id = camera_frame;
        temp_collision_object.id = "object_" + std::to_string(i);
        temp_collision_object.primitives.push_back(msg->object_shapes[i]);
        temp_collision_object.primitive_poses.push_back(msg->object_poses[i].pose);
        PrintPose(msg->object_poses[i]);
        temp_collision_object.operation = temp_collision_object.ADD;
        psw.collision_objects.push_back(temp_collision_object);

      }
      std::cout << "Added objects" <<std::endl;

      // Add object to planning scene
      {  // Lock PlanningScene
        planning_scene_monitor::LockedPlanningSceneRW scene(moveit_cpp_->getPlanningSceneMonitor());
        //scene->is_diff = true;
        scene->processPlanningSceneWorldMsg(psw);
        //scene->processCollisionObjectMsg(collision_object);
      }  // Unlock PlanningScene

      {planning_scene_monitor::LockedPlanningSceneRW scene(moveit_cpp_->getPlanningSceneMonitor());}

      PrintPose(get_object_pose("object_0"));


      std::vector<geometry_msgs::msg::PoseStamped>grasp_pose_vector;
      for(int grasp = 0; grasp < int(msg->grasp_poses.size()); grasp++)
      {
        geometry_msgs::msg::PoseStamped grasp_pose;
        grasp_pose.header.frame_id = robot_frame; 
        grasp_pose = camera_to_robot(msg->grasp_poses[grasp], camera_frame, robot_frame) ;
        geometry_msgs::msg::PoseStamped temp_target_pose = grasp_pose;

        temp_target_pose.pose.position.z = home_pose.pose.position.z;// Initial approach doesnt move down yet 
        PrintPose(temp_target_pose);
        move_to(temp_target_pose, ee_link); //Robot is above the object 

        move_until_before_collide(temp_target_pose, ee_link, -0.005, 100, "z"); //Move down to pick
        //temp_target_pose.pose.position.z = grasp_pose.pose.position.z;
        // int count = 0;
        // while(count < 100)
        // {
        //   if(move_to(temp_target_pose, ee_link))
        //   {
        //     break;
        //   }
        //   std::cout<< " up by 1" <<std::endl;
        //   temp_target_pose.pose.position.z+=0.05;
        //   count++;
        // }
        // count = 0;
        
        moveit_msgs::msg::AttachedCollisionObject current_attached_obj = attach_object_to_ee(grasp,psw);
        rclcpp::sleep_for(std::chrono::seconds(3));

        temp_target_pose.pose.position.z = home_pose.pose.position.z;
        move_to(temp_target_pose, ee_link);
        
        geometry_msgs::msg::PoseStamped release_pose = home_pose;
        release_pose.pose.position.y += 0.2;
        move_until_before_collide(release_pose, ee_link, -0.01, 100, "z"); //move to release

        // while(count < 100)
        // {
        //   std::cout << "get release" <<std::endl;
        //   arm->setGoal(release_pose, ee_link);
        //   const auto plan_solution = arm->plan();
        //   if(plan_solution)
        //   {
        //     release_pose.pose.position.z-=0.01;
        //   }
        //   else
        //   {
        //     release_pose.pose.position.z+=0.01;
        //     move_to(release_pose, ee_link);
        //     break;
        //   }
        //   count++;
        // }

        
        rclcpp::sleep_for(std::chrono::seconds(3));
        detach_object_to_ee(grasp,psw, current_attached_obj);
        rclcpp::sleep_for(std::chrono::seconds(3));
        release_pose.pose.position.z = home_pose.pose.position.z;
        move_to(release_pose, ee_link);
        move_to(home_pose, ee_link); //Robot is above the object 
        

        rclcpp::sleep_for(std::chrono::seconds(3));

        //const std::vector<std::string>& objects = scene->getWorld()->getObjectIds();
        int test;
        std::cin >> test;
      }
      // -------------------------------------------------------------

    }
    geometry_msgs::msg::Pose get_object_pose(std::string object_id)
    {
      geometry_msgs::msg::Pose object_pose;
      {  // Lock PlanningScene
        planning_scene_monitor::LockedPlanningSceneRW scene(moveit_cpp_->getPlanningSceneMonitor());        
        collision_detection::World::ObjectConstPtr object = scene->getWorld()->getObject(object_id);
        auto poses = (*object).shape_poses_[0];
        Eigen::Quaterniond q(poses.linear());
        object_pose.position.x  = poses.translation().x();
        object_pose.position.y  = poses.translation().y();
        object_pose.position.z  = poses.translation().z();
        object_pose.orientation.x  = q.x();
        object_pose.orientation.y  = q.y();
        object_pose.orientation.z  = q.z();
        object_pose.orientation.w  = q.w();        
      }  // Unlock PlanningScene

      {planning_scene_monitor::LockedPlanningSceneRW scene(moveit_cpp_->getPlanningSceneMonitor());}
      return  object_pose;
    }

    geometry_msgs::msg::Pose get_curr_pose(std::string link_name)
    {
      const moveit::core::LinkModel* link = arm->getStartState()->getLinkModel(link_name);
      const Eigen::Isometry3d& transform = arm->getStartState()->getGlobalLinkTransform(link);
      ASSERT_ISOMETRY(transform);  // unsanitized input, could contain a non-isometry
      Eigen::Quaterniond q(transform.linear());
      geometry_msgs::msg::Pose output_pose;
      output_pose.position.x = transform.translation().x();
      output_pose.position.y = transform.translation().y();
      output_pose.position.z = transform.translation().z();
      output_pose.orientation.x = q.x();
      output_pose.orientation.y = q.y();
      output_pose.orientation.z = q.z();
      output_pose.orientation.w = q.w();
      return output_pose;
    }
    void PrintPose(geometry_msgs::msg::PoseStamped pose)
    {
        std::cout << "Grasp Pose frameid : " << pose.header.frame_id <<std::endl;
        std::cout << "Grasp Pose Position" << std::endl;
        std::cout << "X: " <<  pose.pose.position.x<<std::endl;
        std::cout << "Y: " <<  pose.pose.position.y<<std::endl;
        std::cout << "Z: " <<  pose.pose.position.z<<std::endl<<std::endl;

        std::cout << "Grasp Pose Orientation" << std::endl;
        std::cout << "X: " <<  pose.pose.orientation.x<<std::endl;
        std::cout << "Y: " <<  pose.pose.orientation.y<<std::endl;
        std::cout << "Z: " <<  pose.pose.orientation.z<<std::endl;
        std::cout << "W: " <<  pose.pose.orientation.w<<std::endl<<std::endl;
    }

    void PrintPose(geometry_msgs::msg::Pose pose)
    {
        std::cout << "Grasp Pose Position" << std::endl;
        std::cout << "X: " <<  pose.position.x<<std::endl;
        std::cout << "Y: " <<  pose.position.y<<std::endl;
        std::cout << "Z: " <<  pose.position.z<<std::endl<<std::endl;

        std::cout << "Grasp Pose Orientation" << std::endl;
        std::cout << "X: " <<  pose.orientation.x<<std::endl;
        std::cout << "Y: " <<  pose.orientation.y<<std::endl;
        std::cout << "Z: " <<  pose.orientation.z<<std::endl;
        std::cout << "W: " <<  pose.orientation.w<<std::endl<<std::endl;
    }

    geometry_msgs::msg::PoseStamped camera_to_robot(geometry_msgs::msg::PoseStamped input_pose, std::string camera_link, std::string robot_link)
    {
      geometry_msgs::msg::PoseStamped transformed_pose;
      try
      { 
        tf2::doTransform(input_pose, transformed_pose, transformStamped);
      }
      catch (tf2::TransformException &ex) 
      {
        RCLCPP_INFO(LOGGER, ex.what());
      }
      return transformed_pose;
    }

    bool move_to(geometry_msgs::msg::PoseStamped pose, std::string link)
    {
      rclcpp::sleep_for(std::chrono::seconds(3));
      std::cout<<" ---------------------------------------------------------------------------------------------" <<std::endl;
      arm->setGoal(pose, link);
      const auto plan_solution = arm->plan(); //PlanningComponent::PlanSolution
      if (plan_solution)
      {
        RCLCPP_INFO(LOGGER, "Sending the trajectory for execution");
        arm->execute(false);
        std::cout<<" ---------------------------------------------------------------------------------------------" <<std::endl;

        return true;
      }
      else
      {
        std::cout<<" ---------------------------------------------------------------------------------------------" <<std::endl;
        return false;
      }
    }
    bool move_until_before_collide(geometry_msgs::msg::PoseStamped pose, std::string link, float step_size, int max_attempts, std::string axis)
    {
      int count = 0;
      while(count < max_attempts)
      {
        arm->setGoal(pose, link);
        const auto plan_solution = arm->plan();
        if(plan_solution)
        {
          if(axis.compare("x") == 0){pose.pose.position.x+=step_size ;}
          if(axis.compare("y") == 0){pose.pose.position.y+=step_size;}
          if(axis.compare("z") == 0){pose.pose.position.z+=step_size;}
          
        }
        else
        {
          if(axis.compare("x") == 0){pose.pose.position.x-=step_size;}
          if(axis.compare("y") == 0){pose.pose.position.y-=step_size;}
          if(axis.compare("z") == 0){pose.pose.position.z-=step_size;}
          
          move_to(pose,link);
          return true;
        }
        count++;
      }
      return false;
    }

    moveit_msgs::msg::AttachedCollisionObject attach_object_to_ee(int item_num, moveit_msgs::msg::PlanningSceneWorld psw)
    {

      /*
      moveit_msgs::AttachedCollisionObject
      -----------------------------------   
      string link_name
      moveit_msgs/CollisionObject object
      string[] touch_links
      trajectory_msgs/JointTrajectory detach_posture
      float64 weight
      */
      rclcpp::sleep_for(std::chrono::seconds(3));
      geometry_msgs::msg::PoseStamped ee_pose;
      geometry_msgs::msg::PoseStamped cam_pose;
      cam_pose.header.frame_id = camera_frame;
      cam_pose.pose = psw.collision_objects[item_num].pose;
      ee_pose.header.frame_id = "gripper_base_link";

      geometry_msgs::msg::TransformStamped ee_tf = tfBuffer->lookupTransform("gripper_base_link", camera_frame,node_->now(),std::chrono::seconds(1000));
      try
      { 
        tf2::doTransform(cam_pose,ee_pose, ee_tf);
      }
      catch (tf2::TransformException &ex) 
      {
        RCLCPP_INFO(LOGGER, ex.what());
      }


      moveit_msgs::msg::AttachedCollisionObject attached_object;
      attached_object.link_name = "gripper_base_link";
      std::vector<std::string> ignore_links{
        "gripper_finger1_finger_link",
        "gripper_finger1_finger_tip_link",
        "gripper_finger1_inner_knuckle_link",
        "gripper_finger1_knuckle_link",
        "gripper_finger2_finger_link",
        "gripper_finger2_finger_tip_link",
        "gripper_finger2_inner_knuckle_link",
        "gripper_finger2_knuckle_link",
        "gripper_base_link"
      };

      attached_object.touch_links = ignore_links;
      attached_object.object = psw.collision_objects[item_num];
      attached_object.object.pose = ee_pose.pose;
      attached_object.object.operation = attached_object.object.ADD;


      psw.collision_objects.erase(psw.collision_objects.begin() + item_num);
      rclcpp::sleep_for(std::chrono::seconds(1));

      // Add object to planning scene
      {  // Lock PlanningScene
        planning_scene_monitor::LockedPlanningSceneRW scene(moveit_cpp_->getPlanningSceneMonitor());
        scene->processAttachedCollisionObjectMsg(attached_object);
        scene->processPlanningSceneWorldMsg(psw);

      }  // Unlock PlanningScene

      {planning_scene_monitor::LockedPlanningSceneRW scene(moveit_cpp_->getPlanningSceneMonitor());}
      rclcpp::sleep_for(std::chrono::seconds(3));
      return attached_object;
    }

    void detach_object_to_ee(int item_num, moveit_msgs::msg::PlanningSceneWorld psw, moveit_msgs::msg::AttachedCollisionObject attached_object)
    {

      /*
      moveit_msgs::AttachedCollisionObject
      -----------------------------------   
      string link_name
      moveit_msgs/CollisionObject object
      string[] touch_links
      trajectory_msgs/JointTrajectory detach_posture
      float64 weight
      */

      geometry_msgs::msg::PoseStamped ee_pose;
      //geometry_msgs::msg::PoseStamped cam_pose;
      geometry_msgs::msg::PoseStamped cam_pose;
      ee_pose.header.frame_id = "gripper_base_link";
      ee_pose.pose = attached_object.object.pose;
      cam_pose.header.frame_id = camera_frame;

      geometry_msgs::msg::TransformStamped cam_tf = tfBuffer->lookupTransform(camera_frame,"gripper_base_link", node_->now(),std::chrono::seconds(1000));
      try
      { 
        tf2::doTransform(ee_pose,cam_pose, cam_tf);
        //tf2::doTransform(ee_pose,world_pose, cam_tf);
      }
      catch (tf2::TransformException &ex) 
      {
        RCLCPP_INFO(LOGGER, ex.what());
      }

      moveit_msgs::msg::AttachedCollisionObject detach_object;
      attached_object.link_name = "gripper_base_link";
      std::vector<std::string> ignore_links{
        "gripper_finger1_finger_link",
        "gripper_finger1_finger_tip_link",
        "gripper_finger1_inner_knuckle_link",
        "gripper_finger1_knuckle_link",
        "gripper_finger2_finger_link",
        "gripper_finger2_finger_tip_link",
        "gripper_finger2_inner_knuckle_link",
        "gripper_finger2_knuckle_link",
        "gripper_base_link"
        
      };

      detach_object.touch_links = ignore_links;
      detach_object.object.operation = attached_object.object.REMOVE;
      //psw.collision_objects[item_num].pose = get_curr_pose("gripper_base_link");

      rclcpp::sleep_for(std::chrono::seconds(1));

      // Add object to planning scene
      {  // Lock PlanningScene
        planning_scene_monitor::LockedPlanningSceneRW scene(moveit_cpp_->getPlanningSceneMonitor());
        scene->processAttachedCollisionObjectMsg(detach_object);
        //scene->processPlanningSceneWorldMsg(psw);
      }  // Unlock PlanningScene

      {planning_scene_monitor::LockedPlanningSceneRW scene(moveit_cpp_->getPlanningSceneMonitor());}
    }

    private:
    rclcpp::Subscription<grasp_planning::msg::GraspPose>::SharedPtr planning_sub;
    rclcpp::Node::SharedPtr node_;
    rclcpp::Publisher<moveit_msgs::msg::DisplayRobotState>::SharedPtr robot_state_publisher_;
    moveit::planning_interface::MoveItCppPtr moveit_cpp_;
    moveit::planning_interface::PlanningComponent* arm;
    geometry_msgs::msg::TransformStamped transformStamped;
    std::string robot_frame = "base_link";
    std::string camera_frame = "camera_frame";

    tf2_ros::TransformListener* tfListener;
    tf2_ros::Buffer* tfBuffer;
    //moveit::planning_interface::PlanningComponent *arm;

};

int main(int argc, char **argv)
{
  RCLCPP_INFO(LOGGER, "Initialize node");
  rclcpp::init(argc, argv);
  rclcpp::NodeOptions node_options;
  node_options.automatically_declare_parameters_from_overrides(true);
  rclcpp::Node::SharedPtr node = rclcpp::Node::make_shared("grasp_execution_node", "", node_options);

  GraspExecutionNode demo(node);

  rclcpp::sleep_for(std::chrono::seconds(5));

  // std::thread run_demo([&demo]() 
  // {
  //   rclcpp::sleep_for(std::chrono::seconds(5));
  //   demo.run();
  // });
  
  
  // rclcpp::spin(node);
  // run_demo.join();

  demo.run();
  rclcpp::executors::MultiThreadedExecutor executor;
  executor.add_node(node);
  executor.spin();

  rclcpp::shutdown();
  return 0;
}

