
#include "rclcpp/rclcpp.hpp"

#include "rclcpp/rclcpp.hpp"
#include <memory>
#include <chrono>

#include <thread>
#include <moveit/moveit_cpp/moveit_cpp.h>
#include <moveit/moveit_cpp/planning_component.h>
#include <moveit/robot_state/conversions.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit_msgs/msg/display_robot_state.hpp>

#include <trajectory_msgs/msg/joint_trajectory.hpp>
#include <geometry_msgs/msg/pose_stamped.hpp>
#include "control_msgs/action/follow_joint_trajectory.hpp"
#include "rclcpp_action/rclcpp_action.hpp"
#include <grasp_planning/msg/grasp_pose.hpp>

static const rclcpp::Logger LOGGER = rclcpp::get_logger("grasp_execution_node");

class GraspExecutionNode
{
    public:

    GraspExecutionNode(const rclcpp::Node::SharedPtr& node)
    : node_(node)
    , robot_state_publisher_(node_->create_publisher<moveit_msgs::msg::DisplayRobotState>("display_robot_state", 1))
    {
    }
    
    void run()
    {
      moveit_cpp_ = std::make_shared<moveit::planning_interface::MoveItCpp>(node_);
      std::cout << "declared moveit_cpp" << std::endl;
      moveit_cpp_->getPlanningSceneMonitor()->providePlanningSceneService();  // let RViz display query PlanningScene
      std::cout << "declared moveit_cpp" << std::endl;
      moveit_cpp_->getPlanningSceneMonitor()->setPlanningScenePublishingFrequency(100);
      planning_sub  = node_->create_subscription<grasp_planning::msg::GraspPose>("/grasp_poses",10, std::bind(&GraspExecutionNode::execution_init, this, std::placeholders::_1));

    }

    void execution_init(const grasp_planning::msg::GraspPose::SharedPtr msg) const
    {
      moveit::planning_interface::PlanningComponent arm("manipulator", moveit_cpp_);

      //moveit::planning_interface::MoveItCppPtr moveit_cpp_;
      std::cout << "Inside callback" << std::endl;
      
      std::cout<< "Start Run!!!!" << std::endl;
      
      // A little delay before running the plan
      rclcpp::sleep_for(std::chrono::seconds(3));


      // Create collision objects
      moveit_msgs::msg::CollisionObject collision_object;
      collision_object.header.frame_id = "base_link";
      collision_object.id = "box";

      shape_msgs::msg::SolidPrimitive box;
      box.type = box.BOX;
      box.dimensions = { 0.1, 0.1, 0.15 };

      geometry_msgs::msg::Pose box_pose;
      box_pose.position.x = 0;
      box_pose.position.y = 0.5;
      box_pose.position.z = 0.15/2;

      collision_object.primitives.push_back(box);
      collision_object.primitive_poses.push_back(box_pose);
      collision_object.operation = collision_object.ADD;

      // Add object to planning scene
      {  // Lock PlanningScene
        planning_scene_monitor::LockedPlanningSceneRW scene(moveit_cpp_->getPlanningSceneMonitor());
        scene->processCollisionObjectMsg(collision_object);
      }  // Unlock PlanningScene

      {planning_scene_monitor::LockedPlanningSceneRW scene(moveit_cpp_->getPlanningSceneMonitor());}

      auto robot_model_ptr = moveit_cpp_->getRobotModel();
      auto robot_start_state = arm.getStartState() ;
      robot_start_state->printTransforms();
      auto joint_model_group_ptr = robot_model_ptr->getJointModelGroup("manipulator");

      arm.setStartStateToCurrentState();
      geometry_msgs::msg::PoseStamped target_pose1;
      target_pose1.header.frame_id = "base_link";
      target_pose1.pose.orientation.x = 0.5;
      target_pose1.pose.orientation.y = 0.5;
      target_pose1.pose.orientation.z = 0.5;
      target_pose1.pose.orientation.w = 0.5;
      target_pose1.pose.position.x = 0.3;
      target_pose1.pose.position.y = 0.45;
      target_pose1.pose.position.z = 0.27;

      arm.setGoal(target_pose1, "ee_link");
      const auto plan_solution = arm.plan(); //PlanningComponent::PlanSolution
      if (plan_solution)
      {
        //RCLCPP_INFO(LOGGER, "Sending the trajectory for execution");
        arm.execute();
      }
      rclcpp::sleep_for(std::chrono::seconds(10));

      std::cout << "Second goal" << '\n';
      arm.setStartStateToCurrentState();
      geometry_msgs::msg::PoseStamped target_pose2 = target_pose1;
      target_pose2.pose.position.x = -0.3;
      arm.setGoal(target_pose2, "ee_link");
      const auto plan_solution2 = arm.plan(); //PlanningComponent::PlanSolution
      if (plan_solution2)
      {
        //RCLCPP_INFO(LOGGER, "Sending the trajectory for execution");
        arm.execute();
      }

    }
    private:
    rclcpp::Subscription<grasp_planning::msg::GraspPose>::SharedPtr planning_sub;
    rclcpp::Node::SharedPtr node_;
    rclcpp::Publisher<moveit_msgs::msg::DisplayRobotState>::SharedPtr robot_state_publisher_;
    moveit::planning_interface::MoveItCppPtr moveit_cpp_;
    //moveit::planning_interface::PlanningComponent *arm;


};

int main(int argc, char **argv)
{
  RCLCPP_INFO(LOGGER, "Initialize node");
  rclcpp::init(argc, argv);
  rclcpp::NodeOptions node_options;
  node_options.automatically_declare_parameters_from_overrides(true);
  rclcpp::Node::SharedPtr node = rclcpp::Node::make_shared("grasp_execution_node", "", node_options);

  GraspExecutionNode demo(node);
  std::thread run_demo([&demo]() {
    // Let RViz initialize before running demo
    // TODO(henningkayser): use lifecycle events to launch node
    rclcpp::sleep_for(std::chrono::seconds(5));
    demo.run();
  });

  rclcpp::spin(node);
  run_demo.join();
  return 0;
}

